import numpy as np
import matplotlib.pyplot as plt

""" Library for grayscale Morphological Operations
    
    DilEr (Dilation/Erosion)

 """

#structuring_element = np.zeros((3,3))
#structuring_element[1][1] = 4
#structuring_element[0][1] = 2
#structuring_element[1][0] = 2

#structuring_element[1][2] = 2
#structuring_element[2][1] = 2

#structuring_element[2][2] = 2
#structuring_element[0][0] = 2
#structuring_element[2][0] = 2
#structuring_element[0][2] = 2

# basic structuring element shapes
def get_cross_se(shape=(3,3)):
    cross_se = np.zeros(shape)
    # center
    cross_se[1][1] = 4

    cross_se[0][1] = 2
    cross_se[1][0] = 2

    cross_se[1][2] = 2
    cross_se[2][1] = 2
    return cross_se

def get_vertical_line_se(shape=(3,3)):
    vertical_line_se = np.zeros(shape)
    vertical_line_se[0][1] = 2
    # center
    vertical_line_se[1][1] = 2
    vertical_line_se[2][1] = 2
    return vertical_line_se

def get_horizontal_line_se(shape=(3,3)):
    horizontal_line_se = np.zeros(shape)
    horizontal_line_se[1][0] = 2
    #center
    horizontal_line_se[1][1] = 2
    horizontal_line_se[1][2] = 2
    return horizontal_line_se

def get_right_bottom_corner_se(shape=(3,3)):
    right_bottom_corner_se = np.zeros((3,3))
    # center
    right_bottom_corner_se[1][1] = 4
    right_bottom_corner_se[0][1] = 2
    right_bottom_corner_se[1][0] = 2
    return right_bottom_corner_se

def get_full_box_3x3(shape=(3,3)):
    full_box_3x3 = np.zeros((3,3))
    full_box_3x3[0][0] = 2
    full_box_3x3[0][1] = 2
    full_box_3x3[0][2] = 2

    full_box_3x3[1][0] = 2
    # center point
    full_box_3x3[1][1] = 4
    full_box_3x3[1][2] = 2

    full_box_3x3[2][0] = 2
    full_box_3x3[2][1] = 2
    full_box_3x3[2][2] = 2




number = 0

def get_img_array(index):
    f = open('mnist_test_10.csv', 'r')
    lines = f.readlines()
    f.close()

    newarray = lines[index]
    newarray = newarray.split(',')
    number = newarray[0]
    newarray = newarray[1:]
    newarray = np.array(newarray, dtype="int16")
    return newarray.reshape(28, 28)


def dilation(matrixinput, structuring_element=get_cross_se()):
    output = np.zeros_like(matrixinput)
    expandedmatrix = np.insert(matrixinput, 0, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput)+1, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, 0, 0, axis=1)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput[0])+1, 0, axis=1)

    compare = np.zeros((3,3))
    
    for ia in range(len(matrixinput)):
        ia += 1
        if(ia < len(expandedmatrix) - 1):
            for ib in range(len(expandedmatrix[ia])):
                ib += 1
                if(ib < len(expandedmatrix[ia]) - 1):
                    for ic in range(len(compare)):
                        compare[ic] = expandedmatrix[ia-1+ic][ib-1:ib+2]
                    #print ia, ib
                    #print compare                    
                    #print 'dot mult'
                    #print(np.multiply(compare, structuring_element))
                    #print 'has to be a 1?'
                    #print (np.max(np.multiply(compare, structuring_element)) == 1)
                    
                    # hardcodeado para 2 dimensiones
                    # hay que hacerlo flexible para n dimensiones
                    # TODO
                    rows = np.where( structuring_element > 0)[0]
                    cols = np.where( structuring_element > 0)[1]
                    
                    structuring_element_points = [(x, y) for (x, y) in zip(rows, cols)]
                    
                    # check if are there interesting values
                    interesting_values = [compare[row][col] for (row, col) in structuring_element_points]
                    if(np.sum(interesting_values) == 0): continue

                    # compare matrix - structuring_element -> only positive and 0; if -2 -> clip to 0
                    compare_sum_with_structuring_element = np.clip(np.add(compare, structuring_element), 0, None)

                    interesting_values = [compare_sum_with_structuring_element[row][col] for (row, col) in structuring_element_points]

                    output[ia-1][ib-1] = np.max(interesting_values)

                    #output[ia-1][ib-1] = (np.sum(np.multiply(compare, structuring_element)) / np.sum(structuring_element))
  
    return output

def erosion(matrixinput, structuring_element=get_cross_se()):
    output = np.zeros_like(matrixinput)
    expandedmatrix = np.insert(matrixinput, 0, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput)+1, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, 0, 0, axis=1)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput[0])+1, 0, axis=1)

    compare = np.zeros((3,3))
    
    for ia in range(len(matrixinput)):
        ia += 1
        if(ia < len(expandedmatrix) - 1):
            for ib in range(len(expandedmatrix[ia])):
                ib += 1
                if(ib < len(expandedmatrix[ia]) - 1):
                    for ic in range(len(compare)):
                        compare[ic] = expandedmatrix[ia-1+ic][ib-1:ib+2]
                    #print ia, ib                 
                    
                    # hardcodeado para 2 dimensiones
                    # hay que hacerlo flexible para n dimensiones
                    # TODO
                    rows = np.where( structuring_element > 0)[0]
                    cols = np.where( structuring_element > 0)[1]
                    
                    structuring_element_points = [(x, y) for (x, y) in zip(rows, cols)]
                    
                    # compare matrix - structuring_element -> only positive and 0; if -2 -> clip to 0
                    compare_sum_with_structuring_element = np.clip(np.add(compare, -structuring_element), 0, None)

                    interesting_values = [compare_sum_with_structuring_element[row][col] for (row, col) in structuring_element_points]
                    #print np.min(interesting_values)                   
                    #print interesting_values
                    
                    output[ia-1][ib-1] = np.min(interesting_values)
        

    return output

def normalize(matrixinput, max_white_num, power_number=0.4):
    coef =  float(max_white_num) / float(np.max(matrixinput))

    output = np.multiply(matrixinput, coef, dtype='float32')
    
    # esto se hace para levantar los valores bajos y acercarlos a los 255
    # ver curva de f(x) = x ^ 1/2  (funcion raiz)
    power = np.power(output, power_number)
    coef2 = float(max_white_num) / float(np.max(power))
    output = np.multiply(power, coef2)
    return output



# for debugging

# original number


# original img
#arr = get_img_array(3)
#print 'number', number
#plt.imshow(arr, cmap='Greys')
#plt.show()


# different normalize parameters
#arr = dilation(arr)
#arr = normalize(arr, 255, 0.8)
#plt.imshow(arr, cmap='Greys')
#plt.show()

#print arr

# different normalize parameters
#arr = erosion(arr)
#arr = normalize(arr, 255, 0.34)
#plt.imshow(arr, cmap='Greys')
#plt.show()

