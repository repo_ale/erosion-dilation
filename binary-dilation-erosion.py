import numpy as np
import matplotlib.pyplot as plt

structuring_element = np.zeros((3,3))
structuring_element[1][1] = 1
structuring_element[0][1] = 1
structuring_element[1][0] = 1

structuring_element[1][2] = 1
structuring_element[2][1] = 1




def dilation(matrixinput, structuring_element):
    output = np.zeros_like(matrixinput)
    expandedmatrix = np.insert(matrixinput, 0, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput)+1, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, 0, 0, axis=1)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput[0])+1, 0, axis=1)

    compare = np.zeros((3,3))
    
    for ia in range(len(matrixinput)):
        ia += 1
        if(ia < len(expandedmatrix) - 1):
            for ib in range(len(expandedmatrix[ia])):
                ib += 1
                if(ib < len(expandedmatrix[ia]) - 1):
                    for ic in range(len(compare)):
                        compare[ic] = expandedmatrix[ia-1+ic][ib-1:ib+2]
                    #print ia, ib
                    #print compare                    
                    #print 'dot mult'
                    #print(np.multiply(compare, structuring_element))
                    #print 'has to be a 1?'
                    #print (np.max(np.multiply(compare, structuring_element)) == 1)
                    if((np.max(np.multiply(compare, structuring_element)) == 1)):
                        output[ia-1][ib-1] = 1

    return output

def erosion(matrixinput, structuring_element):
    output = np.zeros_like(matrixinput)
    expandedmatrix = np.insert(matrixinput, 0, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput)+1, 0, axis=0)
    expandedmatrix = np.insert(expandedmatrix, 0, 0, axis=1)
    expandedmatrix = np.insert(expandedmatrix, len(matrixinput[0])+1, 0, axis=1)

    compare = np.zeros((3,3))
    
    for ia in range(len(matrixinput)):
        ia += 1
        if(ia < len(expandedmatrix) - 1):
            for ib in range(len(expandedmatrix[ia])):
                ib += 1
                if(ib < len(expandedmatrix[ia]) - 1):
                    for ic in range(len(compare)):
                        compare[ic] = expandedmatrix[ia-1+ic][ib-1:ib+2]
                    #print ia, ib
                    #print compare                    
                    #print 'dot mult'
                    #print(np.multiply(compare, structuring_element))
                    #print 'has to be a 1?'
                    #print (np.sum(np.multiply(compare, structuring_element)) == np.sum(structuring_element))
                    #print "\n"
                    if(np.sum(np.multiply(compare, structuring_element)) == np.sum(structuring_element)):
                        output[ia-1][ib-1] = 1

    return output

#arr = np.zeros((10,10))

"""
arr[0][2] = 1
arr[1][2] = 1
arr[1][1] = 1
arr[2][1] = 1
arr[2][2] = 1
arr[1][3] = 1
arr[2][3] = 1
arr[1][4] = 1
arr[2][4] = 1
arr[1][5] = 1
arr[2][5] = 1
arr[3][1] = 1
arr[3][2] = 1
arr[3][3] = 1
arr[3][4] = 1
arr[3][5] = 1
arr[3][8] = 1
arr[3][9] = 1
arr[4][8] = 1
arr[4][9] = 1
arr[5][8] = 1
arr[5][9] = 1
arr[6][8] = 1
arr[6][9] = 1
arr[6][5] = 1
arr[5][5] = 1
arr[6][6] = 1
arr[5][6] = 1
arr[6][7] = 1
arr[7][5] = 1
arr[7][6] = 1
arr[5][7] = 1
arr[4][7] = 1
arr[3][7] = 1
arr[3][6] = 1
arr[4][6] = 1
"""



f = open('mnist_test_10.csv', 'r')

lines = f.readlines()
f.close()

newarray = lines[4]
newarray = newarray.split(',')
number = newarray[0]
newarray = newarray[1:]
newarray = np.array(newarray, dtype="int16")
arr = newarray.reshape(28, 28)

for ia in range(len(arr)):
    for ib in range(len(arr[ia])):
        if(arr[ia][ib] > 5):
            arr[ia][ib] = 1
        else:
            arr[ia][ib] = 0

print 'number', number


plt.imshow(arr, cmap='Greys')
plt.show()



arr = erosion(arr, structuring_element)

plt.imshow(arr, cmap='Greys')
plt.show()

arr = dilation(newarray.reshape(28, 28), structuring_element)

plt.imshow(arr, cmap='Greys')
plt.show()

#arr = dilation(arr)

#print arr

